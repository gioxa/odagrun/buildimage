# Build image for odagrun

## purpose

This project creates a CentOS docker Image to build [odagrun](https://gitlab.com/gioxa/odagrun/odagrun) project, with the `odagrun`.

Uses [gioxa/imagebuilder-c7](https://microbadger.com/images/gioxa/imagebuilder-c7)] and builds with `odagrun` on `openshift online starter` as **non-root** and **non-privileged**.

## installed Packages

- ca-certificates
- bind-utils
- util-linux

### development packages:

- zlib-devel
- libcurl-devel
- libcjson-devel
- libyaml-devel
- libzip-devel
- bzip2-devel
- openssl-devel
- libgit2-devel
- http-parser-devel
- check-devel
- c-ares-devel
- lksctp-tools-devel

### static libs to compile oc-runner:

- zlib-static
- glibc-static
- libyaml-static
- libcjson-static
- libcurl-static
- openssl-static
- check-static
- http-parser-static
- libgit2-static
- c-ares-static
- bzip2-static
- libarchive-static

### Group install:

- @Development Tools

