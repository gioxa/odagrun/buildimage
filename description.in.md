
[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

# ${ODAGRUN_IMAGE_REFNAME}

${ODAGRUN_IMAGE_TITLE}
 

Custom build image for the [odagrun](https://gitlab.com/gioxa/odagrun/odagrun) project.

## with `make_os.conf`:

```bash

${MAKE_OS_CONFIG}

```

## with `docker_config.yml`:

```yaml

${DOCKER_CONFIG_YML}

```
 
 ---

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter from GitLab-CI*